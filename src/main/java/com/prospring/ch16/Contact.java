package com.prospring.ch16;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by KiS on 06.03.2017.
 */
@Entity
@Table(name = "contact")
public class Contact implements Serializable {
    private Long id;
    private int version;
}
